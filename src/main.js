/* eslint-disable no-unused-vars */
import FPButton from './components/FPButton.vue';

export default {
  install: (app, _options) => {
    app.component('fp-button', FPButton);
  }
};
